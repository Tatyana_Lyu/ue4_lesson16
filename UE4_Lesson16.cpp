﻿#include <iostream>
using namespace std;


int main()
{
	int day = 0;
	cout << "Enter today's date" << "\n";
	cin >> day;
	const int N = 6;
	int array[N][N] = {};
	int index(day % N);
	int sum = 0;
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			cout << array[i][j] << "\t";
		}
		cout << '\n';
	}
	cout << "\n";
	
	for (int j = 0; j < N; j++)
	{
		sum += array[index][j];
	}
	cout << sum << "\n";
}